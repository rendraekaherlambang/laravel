<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
// Route::resource('product', ProductController::class);
Route::get('/product',[ProductController::class,'index']);
Route::post('/product', [ProductController::class, 'store']);
Route::put('/product/{id}', [ProductController::class, 'update']);
Route::delete('/product/{id}', [ProductController::class, 'destroy']);
Route::get('/product/{id}', [ProductController::class, 'show']);

  // Route::get('/product/report',[ProductController::class,'index']) ;
Route::prefix('product')->group(function () {
    Route::get('/',[ProductController::class,'index']) ;
    Route::get('/report',[ProductController::class,'index']);
});

/**
 * Routes API's Authentication User
 * 
 */
Route::post('register', [UserController::class, 'register']);
Route::post('login', [UserController::class, 'login']);